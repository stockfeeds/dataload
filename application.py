# from fastapi import FastAPI

# application = FastAPI()


# @application.get("/")
# async def root():
#     return {"message": "Hello World"}

from flask import Flask

application = Flask(__name__)


@application.route('/')
def hello_world():
    return 'Testing new deployment'


if __name__ == '__main__':
    application.run(debug=True, port=8080)
